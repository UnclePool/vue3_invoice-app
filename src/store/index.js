import { createStore } from 'vuex';
import db from '@/firebase/firebaseInit';

export default createStore({
  state: {
    isInvoiceModal: false,
    isExitModal: false,
    invoices: [],
    currentInvoiceArray: []
  },
  mutations: {
    toggleModal (state) {
      state.isInvoiceModal = !state.isInvoiceModal;
    },
    toggleExitModal (state) {
      state.isExitModal = !state.isExitModal;
    },
    setInvoices (state, payload) {
      state.invoices.push(payload);
    },
    setCurrentInvoice (state, payload) {
      state.currentInvoiceArray = state.invoices.filter((invoice) => invoice.invoiceId === payload);
    }
  },
  actions: {
    async getInvoices ({ state, commit }) {
      try {
        const dataBase = db.collection('invoice');
        const result = await dataBase.get();
        result.forEach(doc => {
          if (!state.invoices.some(invoice => invoice.docId === doc.id)) {
            const data = { docId: doc.id, ...doc.data() };
            commit('setInvoices', data);
          }
        });
      } catch (e) {
        console.warn(e.message);
      }
    }
  },
  modules: {
  }
})

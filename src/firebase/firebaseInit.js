import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyAFaJ_vheaIlxlkEIVL5swRzkvqV2dilP4",
  authDomain: "invoice-app-f4e36.firebaseapp.com",
  projectId: "invoice-app-f4e36",
  storageBucket: "invoice-app-f4e36.appspot.com",
  messagingSenderId: "227600396362",
  appId: "1:227600396362:web:b06632199dda76b79576b3"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();


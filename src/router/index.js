import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/pages/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/Invoice/:invoiceId",
    name: "Invoice",
    component: () => import('@/views/pages/Invoice.vue'),
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
